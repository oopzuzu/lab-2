import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) {
        String strScore;
        int score;
        String strGrade = "Unknown";
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input your score: ");
        strScore = sc.next();
        // System.out.println(strScore);
        score = Integer.parseInt(strScore);
        // System.out.println(score);

        if(score>=80) {
            strGrade = "A";
        } else if(score>=75) {
            strGrade = "B+";
        } else if(score>=70) {
            strGrade = "B";
        } else if(score>=65) {
            strGrade = "C+";
        } else if(score>=60) {
            strGrade = "C";
        } else if(score>=55) {
            strGrade = "D+";
        } else if(score>=50) {
            strGrade = "D";
        } else {
            strGrade = "F";
        }
        System.out.println("Grade: " + strGrade);
    }
}
